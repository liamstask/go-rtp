package main

import (
	"log"

	"bitbucket.org/liamstask/go-rtp/rtp"
)

const (
	rtpAddr  = "127.0.0.1:5220"
	rtcpAddr = "127.0.0.1:5221"
)

func main() {
	log.Println("running...")

    t := rtp.NewTransportUDP(rtpAddr, rtcpAddr)
	s := rtp.NewSession(t)
    s.Start()
	select {}

	log.Println("done")
}
