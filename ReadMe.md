
# go-rtp

An RTP lib for Go. Very incomplete, still exploring.

[![Build Status](https://drone.io/bitbucket.org/liamstask/go-rtp/status.png)](https://drone.io/bitbucket.org/liamstask/go-rtp/latest)

docs: [http://godoc.org/bitbucket.org/liamstask/go-rtp/rtp](http://godoc.org/bitbucket.org/liamstask/go-rtp/rtp)

# testing

/Applications/VLC.app/Contents/MacOS/VLC -vvv MyFavoriteSong.wav --sout '#rtp{dst=127.0.0.1,port=5220}'
