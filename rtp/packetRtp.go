package rtp

import (
	"encoding/binary"
	"errors"
	"log"
	"net"
)

const (
	versionShift  = 6
	versionMask   = 0x3 << versionShift
	paddingMask   = 0x1 << 5
	extensionMask = 0x1 << 4
	csrcCountMask = 0xf

	markerMask      = 0x1 << 7
	payloadTypeMask = 0x7f

	receptionReportCountMask = 0x1f
)

const supportedProtocolVersion = 0x2

const (
	Audio = 1 << 0
	Video = 1 << 1
)

type PayloadType struct {
	Media      int // audio or video
	SampleRate int
	Channels   int
	Name       string
}

var PayloadTypes = map[uint8]*PayloadType{
	0:  {Audio, 8000, 1, "PCMU"},
	3:  {Audio, 8000, 1, "GSM"},
	4:  {Audio, 8000, 1, "G723"},
	5:  {Audio, 8000, 1, "DVI4"},
	6:  {Audio, 16000, 1, "DVI4"},
	7:  {Audio, 8000, 1, "LPC"},
	8:  {Audio, 8000, 1, "PCMA"},
	9:  {Audio, 8000, 1, "G722"},
	10: {Audio, 44100, 2, "L16"},
	11: {Audio, 44100, 1, "L16"},
	12: {Audio, 8000, 1, "QCELP"},
	13: {Audio, 8000, 1, "CN"},
	14: {Audio, 90000, 0, "MPA"},
	15: {Audio, 8000, 1, "G728"},
	16: {Audio, 11025, 1, "DVI4"},
	17: {Audio, 22050, 1, "DVI4"},
	18: {Audio, 8000, 1, "G729"},
	25: {Video, 90000, 0, "CelB"},
	26: {Video, 90000, 0, "JPEG"},
	28: {Video, 90000, 0, "nv"},
	31: {Video, 90000, 0, "H261"},
	32: {Video, 90000, 0, "MPV"},
	33: {Audio | Video, 90000, 0, "MP2T"},
	34: {Video, 90000, 0, "H263"},
}

// Fixed size header for all RTP packets
type RtpHeader struct {
	Version     uint8
	Padding     bool
	Extension   bool
	CsrcCount   uint8 // 0 - 15
	Marker      bool
	PayloadType uint8
	SeqNum      uint16 // init'd randomly to avoid plaintext attacks
	Timestamp   uint32
	SyncSrc     uint32
	CsrcList    []uint32 // len == CsrcCount
}

type RtpPacket struct {
	buf  []byte
	from net.Addr
}

// Extract the fixed size header from the beginning of this packet
func (p *RtpPacket) Header() (*RtpHeader, error) {
	h := &RtpHeader{}

	if len(p.buf) < 12 {
		return nil, errors.New("RtpPacket: len(buf) < size of header")
	}

	b0 := p.buf[0]
	h.Version = (b0 & versionMask) >> versionShift
	h.Padding = (b0 & paddingMask) != 0
	h.Extension = (b0 & extensionMask) != 0
	h.CsrcCount = (b0 & csrcCountMask)
	if h.CsrcCount > 15 {
		log.Println("rtp: CsrcCount > 15:", h.CsrcCount)
	}

	b1 := p.buf[1]
	h.Marker = (b1 & markerMask) != 0
	h.PayloadType = (b1 & payloadTypeMask)

	be := binary.BigEndian
	h.SeqNum = be.Uint16(p.buf[2:])
	h.Timestamp = be.Uint32(p.buf[4:])
	h.SyncSrc = be.Uint32(p.buf[8:])

	// XXX: ensure buf len is safe for given Count
	for i := uint8(0); i < h.CsrcCount; i++ {
		h.CsrcList = append(h.CsrcList, be.Uint32(p.buf[12+i*4:]))
	}

	if !h.IsValid() {
		return nil, errors.New("RtpHeader: invalid")
	}

	return h, nil
}

func (h *RtpHeader) IsValid() bool {
	// according to appendix A.1
	if h.Version != supportedProtocolVersion {
		return false
	}

	if _, ok := PayloadTypes[h.PayloadType]; !ok {
		return false
	}

	// other suggested checks not implemented for now
	return true
}
