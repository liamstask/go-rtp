package rtp

// Transport sends and receives data on the wire
type Transport interface {
	SetHandler(TransportHandler)
	ListenAndServe() error
}

// Transport reports new data to its TransportHandler
type TransportHandler interface {
	HandleRtp(*RtpPacket)
	HandleRtcp(*RtcpPacket)
}
