package rtp

// Handlers define how we pass incoming data to application code

type RtpHandler interface {
	HandleRtp(*RtpPacket)
}

type RtcpHandler interface {
	HandleRtcp(*RtcpPacket)
}

type CtrlEventHandler interface {
	HandleCtrlEvent(*CtrlEvent)
}

type RtpHandlerFunc func(*RtpPacket)

func (f RtpHandlerFunc) HandleRtp(p *RtpPacket) {
	f(p)
}

type RtcpHandlerFunc func(*RtcpPacket)

func (f RtcpHandlerFunc) HandleRtcp(p *RtcpPacket) {
	f(p)
}

type CtrlEventHandlerFunc func(*CtrlEvent)

func (f CtrlEventHandlerFunc) HandleCtrlEvent(e *CtrlEvent) {
	f(e)
}
