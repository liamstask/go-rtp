package rtp

import (
	"encoding/binary"
	"net"
)

// RTCP packet types
const (
	RtcpSenderReport   = 200
	RtcpReceiverReport = 201
	RtcpSdes           = 202
	RtcpBye            = 203
)

// SDES chunk types
const (
	SdesEnd       = iota // end of SDES list, null octet
	SdesCname            // canonical name
	SdesName             // user name
	SdesEmail            // user's electronic mail address
	SdesPhone            // user's phone number
	SdesLoc              // geographic user location
	SdesTool             // name of application or tool
	SdesNote             // notice about the source
	SdesPriv             // private extensions
	SdesH323Caddr        // H.323 callable address
	sdesMax
)

const (
	rtcpFixedHeaderSize = 4
)

type RtcpPacket struct {
	buf  []byte
	from net.Addr
}

// each RTCP packet begins with a fixed header
type RtcpFixedHeader struct {
	Version    uint8
	Padding    bool
	RCount     uint8
	PacketType uint8
	Length     int // in bytes
}

type SenderReport struct {
	Header  *RtcpFixedHeader
	SyncSrc uint32

	// sender info
	NtpTimestamp uint64
	RtpTimestamp uint32
	PacketCount  uint32
	OctetCount   uint32

	ReceptionReports []ReceptionReport
}

// sub-struct within a SenderReport
type ReceptionReport struct {
	Ssrc             uint32
	FracLost         uint8
	PacketsLost      uint32
	HighSeqNum       uint32
	Jitter           uint32
	LastSRTimestamp  uint32
	DelaySinceLastSR uint32
}

type ReceiverReport struct {
	Header  *RtcpFixedHeader
	SyncSrc uint8
}

type SdesPacket struct {
	Header *RtcpFixedHeader
	Chunks []SdesChunk
}

type SdesChunk struct {
	Ssrc  uint32
	Items []SdesItem
}

type SdesItem struct {
	Type uint8
	Text string
}

type ByePacket struct {
	Header *RtcpFixedHeader
	Srcs   []uint32
	Reason string
}

// Extract the fixed size header from this packet starting at pos
func (p *RtcpPacket) Header(pos int) *RtcpFixedHeader {
	h := &RtcpFixedHeader{}

	b := p.buf[pos:]
	b0 := b[0]
	h.Version = (b0 & versionMask) >> versionShift
	h.Padding = (b0 & paddingMask) != 0
	h.RCount = b0 & receptionReportCountMask

	h.PacketType = b[1]
	// translate length to bytes
	wordsMinusOne := binary.BigEndian.Uint16(b[2:])
	h.Length = int((wordsMinusOne + 1) * 4)

	return h
}

// Extract a sender report from this packet starting at pos.
func (p *RtcpPacket) SenderReport(h *RtcpFixedHeader, pos int) *SenderReport {
	s := &SenderReport{
		Header: h,
	}

	// we expect pos to be pointing at the beginning of the packet
	b := p.buf[pos+rtcpFixedHeaderSize:]
	be := binary.BigEndian

	s.SyncSrc = be.Uint32(b[0:])
	s.NtpTimestamp = be.Uint64(b[4:])
	s.RtpTimestamp = be.Uint32(b[12:])
	s.PacketCount = be.Uint32(b[16:])
	s.OctetCount = be.Uint32(b[20:])

	pos = 24
	for i := uint8(0); i < h.RCount; i++ {
		rr := ReceptionReport{
			Ssrc:             be.Uint32(b[pos:]),
			FracLost:         b[pos+4],
			PacketsLost:      be.Uint32(b[pos+4:]) & 0xffffff,
			HighSeqNum:       be.Uint32(b[pos+8:]),
			Jitter:           be.Uint32(b[pos+12:]),
			LastSRTimestamp:  be.Uint32(b[pos+16:]),
			DelaySinceLastSR: be.Uint32(b[pos+5:]),
		}
		s.ReceptionReports = append(s.ReceptionReports, rr)
		pos += 24
	}

	return s
}

// Extract an SDES packet starting at pos
func (p *RtcpPacket) SdesPacket(h *RtcpFixedHeader, pos int) *SdesPacket {
	s := &SdesPacket{
		Header: h,
	}

	// we expect pos to be pointing at the beginning of the packet
	b := p.buf[pos+rtcpFixedHeaderSize:]

	// zero or more chunks, each of which contain zero or more items
	pos = 0
	for i := uint8(0); i < h.RCount; i++ {
		c := SdesChunk{
			Ssrc: binary.BigEndian.Uint32(b[pos:]),
		}
		pos += 4

		for pos < len(b) {
			// XXX: test that we at least have 2 bytes for type and len
			itemtype := b[pos]
			if itemtype == SdesEnd {
				break
			}
			textlen := int(b[pos+1])
			// strings are utf8 encoded
			// XXX: verify textlen is valid
			c.Items = append(c.Items, SdesItem{
				Type: itemtype,
				Text: string(b[pos+2 : pos+2+textlen]),
			})
			pos += 2 + textlen
		}
		s.Chunks = append(s.Chunks, c)
	}

	return s
}

// Extract a BYE packet starting at pos
func (p *RtcpPacket) ByePacket(h *RtcpFixedHeader, pos int) *ByePacket {
	bp := &ByePacket{
		Header: h,
	}

	// we expect pos to be pointing at the beginning of the packet
	b := p.buf[pos+rtcpFixedHeaderSize:]

	pos = 0
	for i := uint8(0); i < h.RCount; i++ {
		bp.Srcs = append(bp.Srcs, binary.BigEndian.Uint32(b[pos:]))
		pos += 4
	}

	// Reason is optional, check if we have extra bytes
	if pos < len(b) {
		reasonlen := int(b[pos])
		reasonend := pos + 1 + reasonlen
		if reasonend < len(b) {
			bp.Reason = string(b[pos+1 : reasonend])
		}
	}

	return bp
}
