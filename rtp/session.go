package rtp

import (
	"log"
	"sync"
)

type Session struct {
	RtpHandler       RtpHandler
	RtcpHandler      RtcpHandler
	CtrlEventHandler CtrlEventHandler
	Transport        Transport

	sync.RWMutex // locks access to state below
	members      map[uint32]*Source
	tp           int     // the last time an RTCP packet was transmitted
	tc           int     // the current time
	tn           int     // the next scheduled transmission time of an RTCP packet
	pmembers     int     // the estimated number of session members at the time tn was last recomputed
	senders      int     // the most current estimate for the number of senders in the session
	rtcpBw       float64 // The target RTCP bandwidth
	weSent       bool    // true if the application has sent data since the 2nd previous RTCP report was transmitted
	avgRtcpSize  float64 // average compound RTCP packet size, in octets, over all RTCP packets sent and received
	initial      bool    // Flag that is true if the application has not yet sent an RTCP packet.
}

type CtrlEvent struct {
	EventType int    // Either a Stream event or a Rtcp* packet type event, e.g. RtcpSR, RtcpRR, RtcpSdes, RtcpBye
	Ssrc      uint32 // the input stream's SSRC
	Index     uint32 // and its index
	Reason    string // Resaon string if it was available, empty otherwise
}

func NewSession(t Transport) *Session {
	s := &Session{
		Transport: t,
		members:   make(map[uint32]*Source),
	}
	t.SetHandler(s)
	return s
}

func (s *Session) Start() error {

	err := s.Transport.ListenAndServe()
	if err != nil {
		return err
	}

	// compute first transmission interval, guess if not yet specified.
	// XXX: ultimately want to calculate this based on an out source that
	//      the user has created, just arbitrary for now
	if s.rtcpBw == 0.0 {
		format := PayloadTypes[0xa]
		s.rtcpBw += float64(format.SampleRate) * float64(format.Channels*8) / 20.
	}
	s.avgRtcpSize = 28 // for SDES

	go s.transmissionIntervalService()

	return nil
}

// a new RTP packet has arrived from our transport
func (s *Session) HandleRtp(p *RtpPacket) {
	// XXX: check if packet is valid
	hdr, err := p.Header()
	if err != nil {
		log.Println(err)
		return
	}

	s.Lock() // ----------------------------

	src, ok := s.members[hdr.SyncSrc]
	if !ok {
		log.Println("new source:", hdr.SyncSrc)
		src = newSource(hdr) // unknown stream, make a new one
		s.members[hdr.SyncSrc] = src
	}

	for _, csrc := range hdr.CsrcList {
		log.Println("Csrc:", csrc)
	}

	src.valid = src.updateSeq(hdr.SeqNum)
	s.Unlock() // --------------------------

	// forward packet to application
	if s.RtpHandler != nil {
		s.RtpHandler.HandleRtp(p)
	}
}

// a new RTCP packet has arrived from our transport.
//
// any RTCP packet can be compound, which means individual packets
// can be stacked back to back in the same transport layer packet.
func (s *Session) HandleRtcp(p *RtcpPacket) {

	plen := len(p.buf)
	s.updateAvgSize(plen)

	for pos := 0; pos < plen; {
		hdr := p.Header(pos)

		switch hdr.PacketType {
		case RtcpSenderReport:
			sr := p.SenderReport(hdr, pos)
			s.handleSenderReport(p, sr)

		case RtcpReceiverReport:
			log.Println("RtcpReceiverReport")

		case RtcpSdes:
			sd := p.SdesPacket(hdr, pos)
			s.handleSdes(p, sd)

		case RtcpBye:
			bp := p.ByePacket(hdr, pos)
			s.handleBye(p, bp)
		}

		pos += hdr.Length
	}

	// dispatch to application
	if s.RtcpHandler != nil {
		s.RtcpHandler.HandleRtcp(p)
	}
}

// a SenderReport has been received.
func (s *Session) handleSenderReport(p *RtcpPacket, sr *SenderReport) {
	log.Println("SenderReport:", sr)

	// XXX: update stats for relevant streams

	for _, rr := range sr.ReceptionReports {
		// pass RRs to application
		log.Println(rr)
	}
}

func (s *Session) handleSdes(p *RtcpPacket, sd *SdesPacket) {
	log.Println("RtcpSdes:", sd)
}

func (s *Session) handleBye(p *RtcpPacket, b *ByePacket) {
	log.Println("RtcpBye:", b)
}

// update avgRtcpSize per section 6.3.3
func (s *Session) updateAvgSize(sz int) {
	s.avgRtcpSize = (1./16.)*float64(sz) + (15./16.)*s.avgRtcpSize
}

func (s *Session) reportCtrlEvent(code int, ssrc, index uint32) {
	if s.CtrlEventHandler != nil {
		e := &CtrlEvent{
			EventType: code,
			Ssrc:      ssrc,
			Index:     index,
		}
		s.CtrlEventHandler.HandleCtrlEvent(e)
	}
}
