package rtp

import (
	// "log"
	"net"
)

type TransportUDP struct {
	Handler  TransportHandler
	RtpAddr  string
	RtcpAddr string
}

const (
	maxPacket = 1500 // reasonable MTU size
)

func NewTransportUDP(rtp, rtcp string) *TransportUDP {
	return &TransportUDP{
		RtpAddr:  rtp,
		RtcpAddr: rtcp,
	}
}

func (t *TransportUDP) SetHandler(h TransportHandler) {
	t.Handler = h
}

// listen for both incoming rtp and rtcp data
func (t *TransportUDP) ListenAndServe() error {

	rtpAddr, err := net.ResolveUDPAddr("udp", t.RtpAddr)
	if err != nil {
		return err
	}

	rtcpAddr, err := net.ResolveUDPAddr("udp", t.RtcpAddr)
	if err != nil {
		return err
	}

	rtpConn, err := net.ListenUDP(rtpAddr.Network(), rtpAddr)
	if err != nil {
		return err
	}

	rtcpConn, err := net.ListenUDP(rtcpAddr.Network(), rtcpAddr)
	if err != nil {
		rtpConn.Close()
		return err
	}

	go t.readRtpPacket(rtpConn)
	go t.readRtcpPacket(rtcpConn)

	return nil
}

func (t *TransportUDP) readRtpPacket(c *net.UDPConn) {

	for {
		// XXX: worth recycling packets?
		//      do some profiling eventually...
		p := &RtpPacket{
			buf: make([]byte, maxPacket),
		}
		n, addr, err := c.ReadFromUDP(p.buf)
		if err != nil {
			if e, ok := err.(net.Error); ok && e.Timeout() {
				continue // just a timeout, try again
			}
			break // real problem, bail for now
		}

		p.buf = p.buf[:n]
		p.from = addr
		t.Handler.HandleRtp(p)
	}

	c.Close()
}

func (t *TransportUDP) readRtcpPacket(c *net.UDPConn) {

	for {
		// XXX: worth recycling packets?
		//      do some profiling eventually...
		p := &RtcpPacket{
			buf: make([]byte, maxPacket),
		}
		n, addr, err := c.ReadFromUDP(p.buf)
		if err != nil {
			if e, ok := err.(net.Error); ok && e.Timeout() {
				continue // just a timeout, try again
			}
			break // real problem, bail for now
		}

		p.buf = p.buf[:n]
		p.from = addr
		t.Handler.HandleRtcp(p)
	}

	c.Close()
}
