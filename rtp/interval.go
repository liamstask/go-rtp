package rtp

import (
	// "log"
	"math"
	"math/rand"
	"time"
)

// RTCP interval calculation constants
const (
	// Minimum average time between RTCP packets from this site (in
	// seconds).  This time prevents the reports from `clumping' when
	// sessions are small and the law of large numbers isn't helping
	// to smooth out the traffic.  It also keeps the report interval
	// from becoming ridiculously small during transient outages like
	// a network partition.
	rtcpIntervalMinTime = 5.

	// Fraction of the RTCP bandwidth to be shared among active
	// senders.  (This fraction was chosen so that in a typical
	// session with one or two active senders, the computed report
	// time would be roughly equal to the minimum report time so that
	// we don't unnecessarily slow down receiver reports.)  The
	// receiver fraction must be 1 - the sender fraction.
	rtcpIntervalSenderBwFrac = 0.25
	rtcpIntervalRcvrBwFrac   = 1 - rtcpIntervalSenderBwFrac

	// To compensate for "timer reconsideration" converging to a
	// value below the intended average.
	rtcpIntervalCompensation = math.E - 1.5
)

// long running service to handle periodic transmission events
func (s *Session) transmissionIntervalService() {

	ticker := time.NewTicker(s.rtcpInterval())

	for {
		select {
		case <-ticker.C:
			// check for stale streams, etc
		}
	}
}

// calculate the interval, at which we should be sending RTCP packets
func (s *Session) rtcpInterval() time.Duration {
	// time is calculated in seconds, as implemented in Appendix A.7

	// XXX: locking around state access
	rtcpBw := s.rtcpBw
	senders := float64(s.senders)
	initial := s.initial
	rtcpMinTime := rtcpIntervalMinTime
	n := float64(len(s.members))

	// Very first call at application start-up uses half the min
	// delay for quicker notification while still allowing some time
	// before reporting for randomization and to learn about other
	// sources so the report interval will converge to the correct
	// interval more quickly.
	if initial {
		rtcpMinTime /= 2
	}

	// Dedicate a fraction of the RTCP bandwidth to senders unless
	// the number of senders is large enough that their share is
	// more than that fraction.
	if senders <= n*rtcpIntervalSenderBwFrac {
		if s.weSent {
			rtcpBw *= rtcpIntervalSenderBwFrac
			n = senders
		} else {
			rtcpBw *= rtcpIntervalRcvrBwFrac
			n -= senders
		}
	}

	// The effective number of sites times the average packet size is
	// the total number of octets sent when each site sends a report.
	// Dividing this by the effective bandwidth gives the time
	// interval over which those packets must be sent in order to
	// meet the bandwidth target, with a minimum enforced.  In that
	// time interval we send one report so this time is also our
	// average time between reports.
	interval := s.avgRtcpSize * n / rtcpBw
	if math.IsNaN(interval) {
		panic("rtcp interval is NaN")
	}
	if interval < rtcpMinTime {
		interval = rtcpMinTime
	}

	// To avoid traffic bursts from unintended synchronization with
	// other sites, we then pick our actual next report interval as a
	// random number uniformly distributed between 0.5*t and 1.5*t.
	interval *= rand.Float64() + 0.5
	interval /= rtcpIntervalCompensation

	// Duration accepts the interval in nanoseconds
	return time.Duration(interval * 1e9)
}
