package rtp

import ()

const (
	rtpSeqMod     = 1 << 16
	maxDropout    = 3000
	maxMisorder   = 100
	minSequential = 2
)

type Source struct {
	ssrc          uint32
	valid         bool
	maxSeq        uint16 // highest seq. number seen
	cycles        uint32 // shifted count of seq. number cycles
	baseSeq       uint32 // base seq number
	badSeq        uint32 // last 'bad' seq number + 1
	probation     uint32 // sequ. packets till source is valid
	received      uint32 // packets received
	expectedPrior uint32 // packet expected at last interval
	receivedPrior uint32 // packet received at last interval
	transit       uint32 // relative trans time for prev pkt
	jitter        uint32 // estimated jitter
}

func newSource(h *RtpHeader) *Source {
	s := &Source{
		ssrc:      h.SyncSrc,
		probation: minSequential,
	}
	s.initSeq(h.SeqNum)
	s.maxSeq = h.SeqNum - 1
	return s
}

func (s *Source) initSeq(seq uint16) {
	// as implemented in Appendix A.1
	s.baseSeq = uint32(seq)
	s.maxSeq = seq
	s.badSeq = rtpSeqMod + 1 // such that seq == bad_seq is false
	s.cycles = 0
	s.received = 0
	s.receivedPrior = 0
	s.expectedPrior = 0
}

// Update Source's seq and return whether the Source is valid
func (s *Source) updateSeq(seq uint16) bool {
	// as implemented in Appendix A.1
	udelta := seq - s.maxSeq

	// Source is not valid until MIN_SEQUENTIAL packets with
	// sequential sequence numbers have been received.
	if s.probation > 0 {
		// packet is in sequence
		if seq == s.maxSeq+1 {
			s.probation--
			s.maxSeq = seq
			if s.probation == 0 {
				s.initSeq(seq)
				s.received++
				return true
			}
		} else {
			s.probation = minSequential - 1
			s.maxSeq = seq
		}
		return false
	}

	if udelta < maxDropout {
		// in order, with permissible gap
		if seq < s.maxSeq {
			// Sequence number wrapped - count another 64K cycle.
			s.cycles += rtpSeqMod
		}
		s.maxSeq = seq

	} else if udelta <= rtpSeqMod-maxMisorder {
		// the sequence number made a very large jump
		if uint32(seq) == s.badSeq {
			// Two sequential packets -- assume that the other side
			// restarted without telling us so just re-sync
			// (i.e., pretend this was the first packet).
			s.initSeq(seq)
		} else {
			s.badSeq = uint32((seq + 1) & (rtpSeqMod - 1))
			return false
		}
	} else {
		// duplicate or reordered packet
	}

	s.received++
	return true
}
